import java.io.File; 
import java.io.FileNotFoundException;
import java.util.Scanner; 
/**
 * Class to Read and Write Files to Computer
 *
 * @author Miriam Alber
 */
public class ReadWriteFiles
{
    /**
     * Method to Read File from Computer
     *
     * @return    The content of the file as string
     */
    public void readFromFile()
    {
        String data_line = "";
        try {
          File myObj = new File("/Users/miri/Desktop/example.txt");
          Scanner myReader = new Scanner(myObj);
          while (myReader.hasNextLine()) {
            data_line = myReader.nextLine();
            System.out.println(data_line);
          }
          myReader.close();
        } catch (FileNotFoundException e) {
          System.out.println("Datei wurde nicht gefunden.");
          e.printStackTrace();
        }
    
    }
}
