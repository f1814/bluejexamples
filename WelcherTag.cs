using System;
using System.Threading;
using System.Collections.Generic;

class WelcherTag
{
    static void Datum()
    {
        //Datumseingabe durch User
        Console.WriteLine("Bitte geben Sie das gewünschte Datum im Format 'DD.MM.YYYY' ein und bestätigen Sie Ihre Eingabe mit 'Enter':\n");
        string Datum = Console.ReadLine();
        Console.WriteLine();

        //Überprüfen auf Korrektheit der Eingabe
        while (Datum.Length != 10)
        {
            Console.WriteLine("Ungültiges Format, bitte geben Sie das Datum im Format 'DD.MM.YYYY' ein!\n");
            Datum = Console.ReadLine();
            Console.WriteLine();
        }

        //Konvertieren der Eingabe in verwertbare Zahlen
        string Tag1 = "";
        string Monat1 = "";
        string Jahr1 = "";

        for(int zl = 0; zl < 10; ++zl)
        {
            if (zl < 2)
            {
                Tag1 += Datum[zl];
            }
            else if (2 < zl && zl < 5)
            {
                Monat1 += Datum[zl];
            }
            else if (zl > 5)
            {
                Jahr1 += Datum[zl];
            }
        }
        
        int Tag = Convert.ToInt32(Tag1);
        int Monat = Convert.ToInt32(Monat1);
        int Jahr = Convert.ToInt32(Jahr1);

        //Überprüfen der Datumsangaben auf Gültigkeit
        while (Jahr < 1)
        {
            Console.WriteLine("Bitte fangen Sie mindestens bei Jahr 1 an!\n");
            Jahr1 = Console.ReadLine();
            Console.WriteLine();
            while (Jahr1.Length != 4)
            {
                Console.WriteLine("Falsche Eingabe, bitte geben Sie NUR das Jahr im Format 'YYYY' ein\n");
                Jahr1 = Console.ReadLine();
                Console.WriteLine();
            }

            Jahr = Convert.ToInt32(Jahr1);
        }

        while (Monat > 12 || Monat < 1)
        {
            Console.WriteLine("Unlogische Monatseingabe, bitte geben Sie einen existierenden Monat ein!\n");
            Monat1 = Console.ReadLine();
            Console.WriteLine();
            while (Monat1.Length != 2)
            {
                Console.WriteLine("Falsche Eingabe, bitte geben Sie NUR den Monat im Format 'MM' ein!\n");
                Monat1 = Console.ReadLine();
                Console.WriteLine();
            }
            Monat = Convert.ToInt32(Monat1);
        }

        string[] Monate = new string[12] { "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember" };

        while ((Tag > 31 || Tag < 1) && (Monat == 1 || Monat == 3 || Monat == 5 || Monat == 7 || Monat == 8 || Monat == 10 || Monat == 12))
        {
            Console.WriteLine("Unlogische Tageseingabe, bitte geben Sie einen gültigen Tag für den Monat " + Monate[Monat - 1] + " ein!\n");
            Tag1 = Console.ReadLine();
            Console.WriteLine();
            while (Tag1.Length != 2)
            {
                Console.WriteLine("Falsche Eingabe, bitte geben Sie NUR den Tag im Format 'DD' ein!\n");
                Tag1 = Console.ReadLine();
                Console.WriteLine();
            }
            Tag = Convert.ToInt32(Tag1);
        }

        while ((Tag > 30 || Tag < 1) && (Monat == 4 || Monat == 6 || Monat == 9 || Monat == 11))
        {
            Console.WriteLine("Unlogische Tageseingabe, bitte geben Sie einen gültigen Tag für den Monat " + Monate[Monat - 1] + " ein!\n");
            Tag1 = Console.ReadLine();
            Console.WriteLine();
            while (Tag1.Length != 2)
            {
                Console.WriteLine("Falsche Eingabe, bitte geben Sie NUR den Tag im Format 'DD' ein!\n");
                Tag1 = Console.ReadLine();
                Console.WriteLine();
            }
            Tag = Convert.ToInt32(Tag1);
        }

        while ((Tag > 29 || Tag < 1) && (Monat == 2) && (Jahr % 4 == 0) && ((Jahr % 100 != 0 || Jahr % 400 == 0)))
        {
            Console.WriteLine("Unlogische Tageseingabe, bitte geben Sie einen gültigen Tag für den Monat " + Monate[Monat - 1] + " ein!\n");
            Tag1 = Console.ReadLine();
            Console.WriteLine();
            while (Tag1.Length != 2)
            {
                Console.WriteLine("Falsche Eingabe, bitte geben Sie NUR den Tag im Format 'DD' ein!\n");
                Tag1 = Console.ReadLine();
                Console.WriteLine();
            }
            Tag = Convert.ToInt32(Tag1);
        }

        while ((Tag > 28 || Tag < 1) && (Monat == 2) && (((Jahr % 4 != 0) || (Jahr % 100 == 0)) ^ (Jahr % 400 == 0)))
        {
            Console.WriteLine("Unlogische Tageseingabe, bitte geben Sie einen gültigen Tag für den Monat " + Monate[Monat - 1] + " ein!\n");
            Tag1 = Console.ReadLine();
            Console.WriteLine();
            while (Tag1.Length != 2)
            {
                Console.WriteLine("Falsche Eingabe, bitte geben Sie NUR den Tag im Format 'DD' ein!\n");
                Tag1 = Console.ReadLine();
                Console.WriteLine();
            }
            Tag = Convert.ToInt32(Tag1);
        }

        Datum = Tag1 + "." + Monat1 + "." + Jahr1;

        //Rechnung

        //Anzahl der durch 400 teilbaren Jahre ermitteln
        int JZ = Jahr - 1;

        int Dif1 = JZ / 400;

        int ZwErg = Dif1 * 146097;

        //Ermittelung Restjahre
        int Rest1 = JZ % 400;

        //Davon Anzahl der durch 100 teilbaren Jahre ermitteln
        int Dif2 = Rest1 / 100;

        ZwErg += Dif2 * 36524;

        //Ermittelung Restjahre
        int Rest2 = Rest1 % 100;

        //Anzahl der durch 4 teilbaren Jahre ermitteln
        int Dif3 = Rest2 / 4;

        ZwErg += Dif3 * 1461;

        //Restanzahl der vollen Jahre ermitteln
        int Dif4 = Rest2 % 4;

        ZwErg += Dif4 * 365;

        //Anzahl der vollen Monate ermitteln
        for (int zl = 1; zl < Monat; ++zl)
        {
            if (zl == 4 || zl == 6 || zl == 9 || zl == 11)
            {
                ZwErg += 30;
            }
            else if ((zl == 2) && (Jahr % 4 == 0) && ((Jahr % 100 != 0) || (Jahr % 400 == 0)))
            {
                ZwErg += 29;
            }
            else if ((zl == 2) && (((Jahr % 4 != 0) || (Jahr % 100 == 0)) ^ (Jahr % 400 == 0)))
            {
                ZwErg += 28;
            }
            else
            {
                ZwErg += 31;
            }

        }

        //Fehlende Tage dazu addieren
        ZwErg += Tag;

        //Wochentag auf Basis der Rechnung ermitteln
        int WT = ZwErg % 7;
        string[] WTs = new string[7] { "Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag" };

        //Ergebnis ausgeben
        Console.WriteLine("ERGEBNIS:\n");
        Console.WriteLine("Der " + Datum + " ist ein " + WTs[WT] + "!\n\n");
    }

    static void Main()
    {

        Console.WriteLine(@"Mit diesem Programm können Sie sich den Wochentag zu einem beliebigen Datum ab dem Jahr 1 berechnen lassen.
Bitte beachten Sie dabei, dass es sich um Berechnungen nach dem Gregoreanischen Kalender handelt,
welcher erst Ende des 16. Jahrhunderts in Europa eingeführt wurde.");

        int Kontrolle = 1;

        while (Kontrolle > 0)
        {
            Datum();

            Console.WriteLine("Wenn Sie noch einen Wochentag prüfen möchten geben Sie bitte 'j' ein und bestätigen Sie mit 'Enter'.");
            Console.WriteLine("Zum Beenden des Programms nur 'Enter' drücken.\n");
            string Eingabe = Console.ReadLine();
            Console.WriteLine();

            if (Eingabe == "j") { }
            else { Kontrolle = 0; }
        }
    }
}