import java.util.Scanner;

public class ReadFromConsole {
    public ReadFromConsole(){
        
    }
    
    public void readFromConsole(){
        String userInput = "";
        String stop = "Ende";
        
        Scanner scanner = new Scanner(System.in);
        
        while(!userInput.equals(stop)) {
            userInput = scanner.next();
            if(userInput.equals(stop)) {
                System.out.println(userInput + "! Programm wird beendet.");
            }
        }
        scanner.close();
        System.out.println("Programm wurde beendet.");
    }
}