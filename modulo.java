import java.util.Scanner;

public class ReadFromConsole {
    public ReadFromConsole(){
        
    }
    
    public void readFromConsole(){
        Scanner scanner = new Scanner(System.in);
        int stop = 0;
        
        int zahlenInput = 100;
        
        System.out.println("Gib eine Zahl ein, diese wird Modulo 2 gerechnet. Gibst du 0 ein, wird das Programm beendet.");
        
        while(zahlenInput != stop) {
          zahlenInput = scanner.nextInt();
          
          System.out.println(zahlenInput + " mod 2 = " + (zahlenInput % 2));
        } 
        
        scanner.close();
        System.out.println("Das Programm wurde beendet.");
    }
}